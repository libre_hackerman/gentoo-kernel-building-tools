#!/bin/bash

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

cd
[ -d ~/$1 ] || exit 1
version=$(echo $1 | cut -d '-' -f 2)

sudo rm -v /boot/*$version* || exit 2
sudo rm -rv /lib/modules/$version-gentoo-gnu || exit 3
sudo grub-mkconfig -o /boot/grub/grub.cfg || exit 4
rm -rf ~/$1 || exit 5
sudo emerge --depclean -a || exit 6
