#!/bin/bash

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

cd ~/$1 || exit 1
KER_VER="$(basename $(pwd) | cut -d '-' -f 2)-gentoo-gnu"

sudo su << EOF
make modules_install || exit 3
make install || exit 4
rm -fv /boot/*old || exit 5
rm -fv /boot/initramfs-$KER_VER.img || exit 5
dracut --kver $KER_VER || exit 6
grub-mkconfig -o /boot/grub/grub.cfg || exit 7
chmod 600 /boot/vmlinuz-* /boot/initramfs-* || exit 8
rm /usr/src/linux
ln -sv "`pwd`" /usr/src/linux
EOF
