#!/bin/bash

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

cd ~/$1 || exit 1
make mrproper

version=$(echo $1 | cut -d '-' -f 2)
if [ `echo $version | tr -cd '.' | wc -c` == 2 ]; then main=`echo ${version%.*}`; else main=$version; fi
url=https://linux-libre.fsfla.org/pub/linux-libre/releases/
if [[ "$version" == *.0 ]]; then version="$main"; fi

[ -f deblob-$main ] || wget $url$version-gnu/deblob-$main
[ -f deblob-$main.sign ] || wget $url$version-gnu/deblob-$main.sign
[ -f deblob-check ] || wget $url$version-gnu/deblob-check
[ -f deblob-check.sign ] || wget $url$version-gnu/deblob-check.sign

gpg --verify deblob-$main.sign deblob-$main || exit 2
gpg --verify deblob-check.sign deblob-check || exit 3

chmod 744 deblob-$main deblob-check
( ./deblob-$main || exit 4 ) | tee deblobbing.log
